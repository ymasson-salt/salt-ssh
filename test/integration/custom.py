def test_package_is_install(host):
    package = host.package("openssh-server")
    assert package.is_installed

def test_configuration_file(host):
    config = host.file("/etc/ssh/sshd_config")
    assert config.mode == 0o600
    assert config.user == "root"
    assert config.group == "root"
#    assert config_file.contains('X11Forwarding yes')
#    assert config_file.contains('MaxAuthTries 5')
#    assert config_file.contains('PermitRootLogin yes')
#    assert config_file.contains('Ciphers aes128-ctr')
#    assert config_file.contains('MACs hmac-sha2-256')
#    assert config_file.contains('ClientAliveInterval 120')
#    assert config_file.contains('ClientAliveCountMax 2')
#    assert config_file.contains('AllowUsers debian@\* centos@\* root@\* vagrant@\*')
#    assert config_file.contains('AllowGroups root vagrant')
#    assert config_file.contains('DenyUsers foo')
#    assert config_file.contains('DenyGroups bar')
#    assert config_file.contains('Banner \/etc\/issue.net')
#    assert config_file.contains('LoginGraceTime 30')
#    assert config_file.contains('MaxSessions 10')

def test_ssh_server_is_listen(host):
    port = host.socket("tcp://0.0.0.0:22")
    assert port.is_listening
