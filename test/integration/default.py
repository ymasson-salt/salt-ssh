def test_package_is_installed(host):
    package = host.package("openssh-server")
    assert package.is_installed

def test_config_file(host):
    config_file = host.file("/etc/ssh/sshd_config")
    assert config_file.is_file
    assert config_file.mode == 0o600
    assert config_file.user == "root"
    assert config_file.group == "root"

def test_socket_listening(host):
    socket = host.socket('tcp://0.0.0.0:22')
    assert socket.is_listening
