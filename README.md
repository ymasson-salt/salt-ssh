# Saltstack state for SSH

## Compatibility
  * Debian 9 and Devuan 2
  * Debian 10 and Devuan 3
  * Debian 11 and Devuan 4
  * CentOS 7
  * FreeBSD 11

## Usage
### ssh
#### Description
  It will install openssh server and ensure the service is running and is enabled.


### ssh.sshd_config
#### Description
It will manage openssh server configuration file using some CIS basic options.
See pillar.example for all parameters.

In case of a parameter is missing, you can use raw option to set your own.
```
  raw: |
    rawconf1 value1
    rawconf2 value2

```

## More informations
https://www.cisecurity.org/cis-benchmarks/
