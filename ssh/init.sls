# vim: ft=sls
{%- from "ssh/map.jinja" import sshd with context %}

# OS support check
{%- if not salt['grains.get']('os_family') in sshd.supported_os %}
'Operating System not supported':
  test.fail_without_changes
{%- else %}


sshd_package:
  pkg.installed:
    - name: {{ sshd.package }}


sshd_configuration_file:
  file.managed:
    - name: /etc/ssh/sshd_config
    - source: salt://ssh/files/sshd_config
    - template: jinja
    - user: {{ sshd.get('user', 'root') }}
    - group: {{ sshd.get('group', 'root') }}
    - mode: '0600'
    - check_cmd: sshd -t -f
    - require:
      - sshd_package

sshd_service:
  service.running:
    - name: {{ sshd.service }}
    - enable: True
    - require:
      - sshd_package
    - watch:
      - sshd_configuration_file

  {%- if sshd is defined and 'trusted_user_ca_keys' in sshd %}
sshd_user_ca:
  file.managed:
    - name: /etc/ssh/trusted-user-ca-keys.pem
    - contents: {{ sshd.trusted_user_ca_keys }}
    - user: {{ sshd.get('user', 'root') }}
    - group: {{ sshd.get('group', 'root') }}
    - mode: 0400
    - watch_in:
      - sshd_service
  {%- endif %}
{%- endif %}
